#include <xdc/std.h>
#ifndef __config__
__FAR__ char local_power_manager_linux_1_24_03_11__dummy__;
#define __xdc_PKGVERS null
#define __xdc_PKGNAME local_power_manager_linux_1_24_03_11
#define __xdc_PKGPREFIX local_power_manager_linux_1_24_03_11_
#ifdef __xdc_bld_pkg_c__
#define __stringify(a) #a
#define __local_include(a) __stringify(a)
#include __local_include(__xdc_bld_pkg_c__)
#endif

#else

#endif
