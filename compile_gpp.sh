unset LDFLAGS

function green_echo ()
{
    echo -e "\033[32m$1\033[0m"
}

function red_echo ()
{
    echo -e "\033[31m$1\033[0m"
}

function check_error ()
{
    if [ "$?" != "0" ]; then
        red_echo "Erreur, arret"
        exit 1
    fi
}

green_echo "Compilation lpm"
cd $TI_TOOLS_BASE_DIR
#cp local_power_manager/packages/ti/bios/power/utils/bin/ti_platforms_evm3530/linux/release/lpmOFF.x470uC $IGEP_ROOT/uav_dev/bin/arm
#cp local_power_manager/packages/ti/bios/power/utils/bin/ti_platforms_evm3530/linux/release/lpmON.x470uC $IGEP_ROOT/uav_dev/bin/arm
cd local_power_manager/packages/ti/bios/power/modules/omap3530/lpm
make clean
make > /dev/null
check_error
#cp lpm_omap3530.ko $IGEP_ROOT/uav_dev/bin/arm
